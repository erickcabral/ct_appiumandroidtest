package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.MainPage;
import br.toxodev.pageObjects.SubAppAccountTabPage;
import br.toxodev.pageObjects.SubAppLoginPage;
import br.toxodev.pageObjects.SubAppTabsPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Logger;

public class SubAppTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> SubAppTest <<<");

    private String username = "user@user.com", password = "pass123";
    private MainPage mainPage = new MainPage();

    private SubAppLoginPage loginPage = new SubAppLoginPage();
    private SubAppTabsPage subAppTabsPage = new SubAppTabsPage();
    private SubAppAccountTabPage subAppAccountTab = new SubAppAccountTabPage();

    @Before
    public void setup() {
        this.mainPage.selectSubAppNative();
        this.loginPage.logUser(username, password);
    }

    //@Test
    public void loginUserSuccess() {
        this.loginPage.logUser(username, password);
        Assert.assertTrue(this.loginPage.isWarningHidden());
    }

    @Test
    public void addingAccount() {
        //ADDING ACCOUNT
        //click CONTAS Tab
        this.subAppTabsPage.selectAccountTab();
        //Write new account name
        this.subAppAccountTab.insertNewAccountName();
        //Click Save Button
        this.subAppAccountTab.clickSaveButton();
        //Assert Returning Message
        Assert.assertTrue(this.subAppAccountTab.isAccountAdded());
    }

    @Test
    public void deleteAccount() {
        //DELETING ACCOUNT....
        this.subAppTabsPage.selectAccountTab();
        //Write new account name
        this.subAppAccountTab.selectAccountToDelete();
        //Assert Account is Selected
        Assert.assertTrue(this.subAppAccountTab.isAccountSelected());
        //Click Save Button
        this.subAppAccountTab.clickDeleteButton();
        //Assert Returning Message
        Assert.assertTrue(this.subAppAccountTab.isAccountDeleted());
    }

}
