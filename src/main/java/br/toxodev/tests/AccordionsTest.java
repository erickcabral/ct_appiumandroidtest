package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.AccordionsPage;
import br.toxodev.pageObjects.MainPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;

public class AccordionsTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> AccordionsTest <<<");

    private MainPage mainPage = new MainPage();
    private AccordionsPage accordionsPage = new AccordionsPage();

    @Test
    public void accordinonsHandling() {
        //Select Accordion on Menu
        this.mainPage.selectAccordionOption();

        //Click Option 1
        this.accordionsPage.selectOption1();
        //Assert Accordion Content
        Assert.assertTrue(this.accordionsPage.isAccordion1());

        //Click Option 2
        this.accordionsPage.selectOption2();

        //Assert Accordion Content
        Assert.assertTrue(this.accordionsPage.isAccordion2());

        //Click Option 3
        this.accordionsPage.selectOption3();
        //Assert Accordion Content
        Assert.assertTrue(this.accordionsPage.isAccordion3());
        //Click Option 4
        this.accordionsPage.selectOption4();
        //Assert Accordion Content
        Assert.assertTrue(this.accordionsPage.isAccordion4());
    }
}
