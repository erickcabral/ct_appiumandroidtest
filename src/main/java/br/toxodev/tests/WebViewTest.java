package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.MainPage;
import br.toxodev.pageObjects.WebViewPage;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class WebViewTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> WebviewTest <<<");

    private MainPage mainPage = new MainPage();
    private WebViewPage webViewPage = new WebViewPage();

    @Before
    public void setup() {
        this.mainPage.selectWebViewApp();
    }

    @Test
    public void registerUser() {
        //Change context to WebDriver
        try {
            LOG.info(">>> WAITING WEB PAGE TO SET 1 <<<<<");
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.webViewPage.initializeWebDriver();
        //insert User Email
        this.webViewPage.insertUserEmail();
        //insert User Password
//        this.webViewPage.insertUserPass();
        //Click Enter Button
//        this.webViewPage.clickEnterButton();
        //Assert Message
    }
}
