package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.MainPage;
import br.toxodev.pageObjects.SplashPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;

public class SplashScreenTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> SplashScreenTest <<<");

    private MainPage mainPage = new MainPage();
    private SplashPage splashPage = new SplashPage();

    @Test
    public void waitSplashScreenGone() {
        this.mainPage.selectSplash();
        this.splashPage.isSplashScreenShowing();
        this.splashPage.waitSplashGone();
        Assert.assertTrue(this.splashPage.isMenuPageShowing());
    }
}
