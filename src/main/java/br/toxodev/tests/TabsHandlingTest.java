package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.MainPage;
import br.toxodev.pageObjects.TabsPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;

public class TabsHandlingTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> TabsHandlingTest <<<");

    private MainPage mainPage = new MainPage();
    private TabsPage tabsPage = new TabsPage();

    @Test
    public void tabsHandlingTest() {
        //Select Tabs on Menu
        this.mainPage.selectTabOption();
        //Assert Tab 1 is selected
        Assert.assertTrue(this.tabsPage.isTab1Selected());
        //Click Tab 2
        this.tabsPage.selectTab2();
        //Assert Tab 2 is selected
        Assert.assertTrue(this.tabsPage.isTab2Selected());
    }
}
