package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.dsl.DSL;
import br.toxodev.pageObjects.FormularioPage;
import br.toxodev.pageObjects.MainPage;
import io.appium.java_client.MobileBy;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.logging.Logger;

import static org.junit.Assert.*;

public class FormPageTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> AppiumAndroidTests <<<");
    private DSL dsl = new DSL();

    private MainPage mainPage = new MainPage();
    private FormularioPage formularioPage = new FormularioPage();

    @Before
    public void initializeDriver() {
        this.mainPage.selectFormulario();
    }

    @Test
    public void selectFormulario() {
        this.formularioPage.writeUserName("Fofiuvlissis");
        assertEquals("Fofiuvlissis", this.formularioPage.readUserName());
    }

    @Test
    public void comboBoxHandling() {
        this.formularioPage.selectSpinnerObject("PS4");
        assertEquals("PS4", this.formularioPage.getSpinnerSelection());
    }

    @Test
    public void checkBoxAndSwitchHandling() {
        //First Asserting
        assertFalse(this.formularioPage.isCheckBoxChecked());
        assertTrue(this.formularioPage.isSwitchOn());

        //Clicking
        this.formularioPage.clickCheckBox();
        this.formularioPage.clickSwitch();

        //Asserting
        assertTrue(this.formularioPage.isCheckBoxChecked());
        assertFalse(this.formularioPage.isSwitchOn());

    }

    @Test
    public void registrationTest() {
        String name = "newUserName";
        String console = "ps4";
        //Interacting
        this.dsl.sendKeys(By.xpath("//android.widget.EditText[@text='Nome']"), name);
        this.dsl.selectComboBoxOption(MobileBy.AccessibilityId("console"), "PS4");

        this.dsl.clickElement(By.xpath("//android.widget.CheckBox"));
        this.dsl.clickElement(By.xpath("//android.widget.Switch"));
        this.dsl.clickElementByText("SALVAR");

        this.formularioPage.writeUserName(name);
        this.formularioPage.selectSpinnerObject("PS4");
        this.formularioPage.clickCheckBox();
        this.formularioPage.clickSwitch();
        //Asserting
        assertEquals(String.format("Nome: %s", name), this.formularioPage.getRegisteredUsername());
        assertEquals(String.format("Console: %s", console), this.formularioPage.getRegisteredConsoleText());
        assertEquals("Switch: Off", this.formularioPage.getRegisteredSwitchText());
        assertEquals("Checkbox: Marcado", this.formularioPage.getRegisteredCheckBoxText());
    }

    @Test
    public void datePickerHandlgin() {
        //Select Date Element
        this.formularioPage.selectDatePicker();
        //Select Date XX
        this.formularioPage.selectDay();
        //Click OK Button
        this.formularioPage.confirmDate();
        //Assert Date Selected
        assertTrue(this.formularioPage.isSelectedDateCorrect());
    }

    @Test
    public void timePickerHandling() {
        //Select Date Element
        this.formularioPage.selectTimePicker();
        //Select Date XX
        this.formularioPage.selectHour();
        this.formularioPage.selectMinutes();
        //Click OK Button
        this.formularioPage.confirmTime();
        //Assert Date Selected
        assertTrue(this.formularioPage.isTimeSelectedShowing());
    }
}
