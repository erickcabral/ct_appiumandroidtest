package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.MainPage;
import br.toxodev.pageObjects.SwipeAndScrollPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Logger;

public class SwipeScrollTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> SwipeScrollTest <<<");

    private MainPage mainPage = new MainPage();
    private SwipeAndScrollPage swipeAndScrollPage = new SwipeAndScrollPage();

    @Before
    public void setup() {
        this.mainPage.scrollToLastOption();
    }

    @Test
    public void selectLastOption() {
        //Assert Option is on Screen
        Assert.assertTrue(this.mainPage.isLastOptionShowing());
        //CLick LastOption
        this.mainPage.selectLastOption();
        //Assert Alert
        Assert.assertTrue(this.mainPage.isLastOptionClicked());
    }

    @Test
    public void swipeHandling() {
        //Click Swipe Option
        this.mainPage.selectSwipeOption();
        //Assert 1st Screen
        Assert.assertTrue(this.swipeAndScrollPage.isFirstScreen());
        //Swipe left
        this.swipeAndScrollPage.swipePageLeft();
        //Assert 2nd Screen
        Assert.assertTrue(this.swipeAndScrollPage.isSecondScreen());
        //Swipe left
        this.swipeAndScrollPage.swipeLeft();
        //Assert 3rd Screen
        Assert.assertTrue(this.swipeAndScrollPage.isThirdScreen());
    }
}
