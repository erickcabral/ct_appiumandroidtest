package br.toxodev.tests;

import br.toxodev.baseClasses.BaseTest;
import br.toxodev.pageObjects.AlertPage;
import br.toxodev.pageObjects.MainPage;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;

public class AlertTest extends BaseTest {
    private final Logger LOG = Logger.getLogger(">>> AlertTest <<<");

    private MainPage mainPage = new MainPage();
    private AlertPage alertPage = new AlertPage();

    @Test
    public void confirmAlerts() {
        //Click Alert on Menu
        this.mainPage.selectAlertOption();
        //Click Alert Button
        this.alertPage.selectAlertConfirm();
        //Assert Alert Title and Text
        Assert.assertEquals("Info", this.alertPage.getAlertTitle());
        Assert.assertEquals("Confirma a operação?", this.alertPage.getAlertText());
        //Click Confirm Button
        this.alertPage.clickConfirmButton();
        //Assert Alert Title and Text
        Assert.assertEquals("Info", this.alertPage.getAlertTitle());
        Assert.assertEquals("Confirmado", this.alertPage.getAlertText());
        //Click Exit Button
        this.alertPage.clickExit();
    }
}
