package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import br.toxodev.driverFactory.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SplashPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> SplashScreen <<<");

    public boolean isSplashScreenShowing() {
        return isElementPresentByText("Splash!");
    }

    public void waitSplashGone() {
        DriverFactory.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //Turning ImplicitWait OFF
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10); //Creating Explicit Wait
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@text='Splash!']")));
        DriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //Turning ImplicitWait ON Again...
    }

    public boolean isMenuPageShowing() {
        return isElementPresentByText("Formulário");
    }
}
