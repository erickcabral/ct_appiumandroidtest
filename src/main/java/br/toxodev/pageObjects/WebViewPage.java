package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import org.openqa.selenium.By;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static br.toxodev.driverFactory.DriverFactory.getDriver;

public class WebViewPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> WebViewPage <<<");

    private String username = "user@user.com", password = "pass123";

    public void initializeWebDriver() {
        try {
            LOG.info(">>> WAITING WEB PAGE TO SET <<<<<");
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        LOG.info(">>>> GETTING CONTEXTS <<<<");
        Set<String> contextHandles = getDriver().getContextHandles();
        for (String context : contextHandles) {
            LOG.info(">>> CONTEXT > " + context);
        }
        LOG.info(">>> DEVERIA EXISTIR >>>> WEBVIEW_com.ctappium.MainActivity <<<");
        getDriver().context("WEBVIEW_com.ctappium.MainActivity");
    }


    public void insertUserEmail() {
        sendKeys(By.id("email"), username);
//        sendKeys(By.xpath("//form-control[@id='email']"), username);
    }

    public void insertUserPass() {
        sendKeys(By.id("senha"), password);
    }

    public void clickEnterButton() {
        clickElement(By.className("btn btn-primary"));
    }
}
