package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;

import java.util.logging.Logger;

public class SwipeAndScrollPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> SwipesPage <<<");

    public boolean isFirstScreen() {
        return isElementPresentByText("a esquerda");
    }

    public boolean isSecondScreen() {
        return isElementPresentByText("você consegue");
    }

    public boolean isThirdScreen() {
        return isElementPresentByText("Chegar até o fim!");
    }

    public void swipePageLeft() {
        swipeLeft();
    }
}
