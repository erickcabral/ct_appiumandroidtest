package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import org.openqa.selenium.By;

import java.util.logging.Logger;

public class SubAppLoginPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> SubAppLoginPage <<<");

    public void insertUserName(String username) {
        sendKeys(By.xpath("//android.widget.EditText[@text='Nome']"), username);
    }

    public void insertPassword(String pass) {
        sendKeys(By.xpath("//android.widget.EditText[@text='Senha']"), pass);
    }

    public void clickLoginButton() {
        clickElementByText("ENTRAR");
    }

    public void logUser(String username, String password) {
        this.insertUserName(username);
        this.insertPassword(password);
        this.clickLoginButton();
    }

    public boolean isWarningHidden() {
        return !isElementPresentByText("Problemas com o login");
    }
}
