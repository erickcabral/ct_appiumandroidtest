package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

import java.util.logging.Logger;

public class FormularioPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> FormularioPage <<<");

//    private DSL dsl = new DSL();

    public void writeUserName(String name) {
        sendKeys(MobileBy.AccessibilityId("nome"), name);
    }

    public String readUserName() {
        return getObjectText(MobileBy.AccessibilityId("nome"));
    }

    public void selectSpinnerObject(String text) {
        clickElement(MobileBy.AccessibilityId("console"));
        clickElementByText(text);
    }

    public String getSpinnerSelection() {
        return getObjectText(By.xpath("//android.widget.Spinner/android.widget.TextView"));
    }

    public boolean isCheckBoxChecked() {
        return isChecked(By.xpath("//android.widget.CheckBox"));
    }

    public boolean isSwitchOn() {
        return isChecked(By.xpath("//android.widget.Switch"));
    }

    public void clickCheckBox() {
        clickElement(By.xpath("//android.widget.CheckBox"));
    }

    public void clickSwitch() {
        clickElement(By.xpath("//android.widget.Switch"));
    }

    public String getRegisteredUsername() {
        return getObjectText(By.xpath("//android.widget.TextView[starts-with(@text,'Nome:')]"));
    }

    public String getRegisteredConsoleText() {
        return getObjectText(By.xpath("//android.widget.TextView[starts-with(@text,'Console:')]"));
    }

    public String getRegisteredSwitchText() {
        return getObjectText(By.xpath("//android.widget.TextView[starts-with(@text,'Switch:')]"));
    }

    public String getRegisteredCheckBoxText() {
        return getObjectText(By.xpath("//android.widget.TextView[starts-with(@text,'Checkbox:')]"));
    }

    public void selectDatePicker() {
        clickElementByText("01/01/2000");
    }

    public void selectDay() {
        clickElementByText("18");
    }

    public boolean isSelectedDateCorrect() {
        return isElementPresentByText("18/2/2000");
    }

    public void confirmDate() {
        clickElement(By.id("android:id/button1"));
    }

    public void selectTimePicker() {
        clickElementByText("06:00");
    }

    public void selectHour() { // 20h
        clickElement(MobileBy.AccessibilityId("20"));
    }

    public void selectMinutes() {
        clickElement(MobileBy.AccessibilityId("55"));
    }

    public void confirmTime() {
        clickElement(By.id("android:id/button1"));
    }

    public boolean isTimeSelectedShowing() {
        return isElementPresentByText("20:55");
    }
}
