package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import br.toxodev.driverFactory.DriverFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class MainPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> MainActivity <<<");

//    private DSL dsl = new DSL();


    public MainPage() {
        AndroidDriver<MobileElement> driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.TextView")));
        LOG.info(">>>>>>>>> MAIN PAGE INITIALIZED <<<<<<<<<<<");
    }

    public void selectFormulario() {
        clickElement(By.xpath("//android.widget.TextView[@text='Formulário']"));
    }

    public void selectSplash() {
        clickElement(By.xpath("//android.widget.TextView[@text='Splash']"));
    }

    public void selectAlertOption() {
        clickElement(By.xpath("//android.widget.TextView[@text='Alertas']"));
    }

    public void selectTabOption() {
        clickElement(By.xpath("//android.widget.TextView[@text='Abas']"));
    }

    public void selectAccordionOption() {
        clickElement(By.xpath("//android.widget.TextView[@text='Accordion']"));
    }

    public void selectSubAppNative() {
        clickElement(By.xpath("//android.widget.TextView[@text='SeuBarriga Nativo']"));
    }

    public void scrollToLastOption() {
        scrollDown();
    }

    public void selectLastOption() {
        clickElementByText("Opção bem escondida");
    }

    public boolean isLastOptionShowing() {
        return isElementPresentByText("Opção bem escondida");
    }

    public boolean isLastOptionClicked() {
        WebDriverWait waitAlert = new WebDriverWait(DriverFactory.getDriver(), 10);
        waitAlert.until(ExpectedConditions.alertIsPresent());
        return isElementPresentByText("Você achou essa opção");
    }

    public void selectSwipeOption() {
        clickElementByText("Swipe");
    }

    public void selectWebViewApp() {
        clickElementByText("SeuBarriga Híbrido");
    }
}
