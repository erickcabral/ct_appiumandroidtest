package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;

import java.util.logging.Logger;

public class AccordionsPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> AccordionsPage <<<");

    public void selectOption1() {
        clickElementByText("Opção 1");
    }

    public void selectOption2() {
        clickElementByText("Opção 2");
    }

    public void selectOption3() {
        clickElementByText("Opção 3");
    }

    public void selectOption4() {
        clickElementByText("Opção 4");
    }

    public void selectOption5() {
        clickElementByText("Opção 5");
    }

    public boolean isAccordion1() {
        return isElementPresentByText("Esta é a descrição da opção 1");
    }

    public boolean isAccordion2() {
        return isElementPresentByText("Esta é a descrição da opção 2");
    }

    public boolean isAccordion3() {
        return isElementPresentByText("Esta é a descrição da opção 3. Que pode, inclusive ter mais que uma linha");
    }

    public boolean isAccordion4() {
        return isElementPresentByText("Esta é a descrição da opção 4");
    }
}
