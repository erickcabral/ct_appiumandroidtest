package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

import static br.toxodev.driverFactory.DriverFactory.getDriver;


public class SubAppAccountTabPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> SubAppAccountTab <<<");

    String accountName = "Test Account";

    public void insertNewAccountName() {
        sendKeys(By.xpath("//android.widget.EditText[@text='Conta']"), accountName);
    }

    public void clickSaveButton() {
        clickElementByText("SALVAR");
    }

    public void clickDeleteButton() {
        clickElementByText("EXCLUIR");
    }

    public boolean isAccountAdded() {
        return isElementPresentByText("Conta adicionada com sucesso");
    }

    public void selectAccountToDelete() {
        MobileElement accountToDelete = getDriver().findElement(By.xpath("//*[@text='" + accountName + "']"));
        LOG.info(String.format(">>>>>> ACCOUNT ELEMENT >>>> %s | %s", accountToDelete.getText(), accountToDelete.getClass().getSimpleName()));

        WebElement deleteButton = getDriver().findElement(By.xpath("//android.widget.Button[1]"));
        new TouchAction(getDriver())
                .longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(accountToDelete)))
                .waitAction()
                .perform();
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        wait.until(ExpectedConditions.attributeToBe(deleteButton, "enabled", "true"));
    }

    public boolean isAccountSelected() {
        WebElement editText = getDriver().findElement(By.className("android.widget.EditText"));
        LOG.info(">>>>>>>>> EDIT TEXT TEXT >>>>>>>> " + editText.getText());
        return editText.getText().equals(accountName);
    }

    public boolean isAccountDeleted() {
        return isElementPresentByText("Conta excluída com sucesso");
    }
}
