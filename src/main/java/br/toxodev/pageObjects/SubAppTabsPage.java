package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;

import java.util.logging.Logger;

public class SubAppTabsPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> SubAppTabsPage <<<");

    public void selectHomeTab() {
        clickElementByText("HOME");
    }

    public void selectAccountTab() {
        clickElementByText("CONTAS");
    }
}
