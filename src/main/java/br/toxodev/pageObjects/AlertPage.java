package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import org.openqa.selenium.By;

import java.util.logging.Logger;

public class AlertPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> AlertPage <<<");

    public void selectAlertConfirm() {
        clickElementByText("ALERTA CONFIRM");
    }

    public String getAlertTitle() {
        return getObjectText(By.id("android:id/alertTitle"));
    }

    public String getAlertText() {
        return getObjectText(By.id("android:id/message"));
    }

    public void clickConfirmButton() {
        clickElementByText("Confirmar");
    }

    public void clickExit() {
        clickElementByText("Sair");
    }
}
