package br.toxodev.pageObjects;

import br.toxodev.baseClasses.BasePage;
import org.openqa.selenium.By;

import java.util.logging.Logger;

public class TabsPage extends BasePage {
    private final Logger LOG = Logger.getLogger(">>> TabsPage <<<");


    public boolean isTab1Selected() {
        return isElementPresentByText("Este é o conteúdo da Aba 1");
    }

    public void selectTab2() {
        clickElement(By.xpath("//android.widget.TextView[@text='ABA 2']"));
    }

    public boolean isTab2Selected() {
        return isElementPresentByText("Este é o conteúdo da Aba 2");
    }
}
