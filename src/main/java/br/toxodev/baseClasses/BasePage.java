package br.toxodev.baseClasses;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import java.time.Duration;
import java.util.List;
import java.util.logging.Logger;

import static br.toxodev.driverFactory.DriverFactory.getDriver;

public class BasePage {
    private final Logger LOG = Logger.getLogger(">>> BasePage <<<");

    private static Dimension SCREEN_SIZE = getDriver().manage().window().getSize();
    private static int SCREEN_WIDTH = SCREEN_SIZE.getWidth();
    private static int SCREEN_HEIGHT = SCREEN_SIZE.getHeight();

    public BasePage() {
        getDriver();
        SCREEN_SIZE = getDriver().manage().window().getSize();
        SCREEN_WIDTH = SCREEN_SIZE.getWidth();
        SCREEN_HEIGHT = SCREEN_SIZE.getHeight();
    }

    public void clickElement(By by) {
        getDriver().findElement(by).click();
    }

    public void sendKeys(By by, String text) {
        getDriver().findElement(by).sendKeys(text);
    }

    public String getObjectText(By by) {
        return getDriver().findElement(by).getText();
    }

    public void clickElementByText(String text) {
        clickElement(By.xpath("//*[@text='" + text + "']"));
    }

    public boolean isChecked(By by) {
        return getDriver().findElement(by).getAttribute("checked").equals("true");
    }

    public void selectComboBoxOption(By comboBox, String option) {
        this.clickElement(comboBox);
        clickElement(By.xpath("//android.widget.CheckedTextView[@text='" + option + "']"));
    }

    public boolean isElementPresentByText(String text) {
        List<MobileElement> elements = getDriver().findElements(By.xpath("//*[@text='" + text + "']"));
        return elements.size() > 0;
    }

    public void scrollDown() {
        int startXSpot = SCREEN_WIDTH / 2;
        int startYSpot = (int) (SCREEN_HEIGHT * 0.85);
        int endYSpot = (int) (SCREEN_HEIGHT * 0.15);
        LOG.info(String.format(">>> INITIAL POINT X: %d | Y: %d", startXSpot, startYSpot));
        LOG.info(String.format(">>> SCROLL ACTION From: %d To: %d", startYSpot, endYSpot));

        new TouchAction<>(getDriver())
                .press(PointOption.point(new Point(startXSpot, startYSpot)))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(new Point(startXSpot, endYSpot)))
                .release()
                .perform();
    }

    public void swipeLeft() {
        final Dimension SCREEN_SIZE = getDriver().manage().window().getSize();
        final int SCREEN_WIDTH = SCREEN_SIZE.getWidth();
        final int SCREEN_HEIGHT = SCREEN_SIZE.getHeight();

        int startYpoint = SCREEN_HEIGHT / 2;
        int startXpoint = (int) (SCREEN_WIDTH * 0.9);
        LOG.info(String.format(">>> INITIAL POINT X: %d | Y: %d", startXpoint, startYpoint));
        int endXpoint = (int) (SCREEN_WIDTH * 0.1);
        LOG.info(String.format(">>> SWIPE ACTION X1: %d | X2: %d", startXpoint, endXpoint));
        new TouchAction<>(getDriver())
                .press(PointOption.point(startXpoint, startYpoint))
                .moveTo(PointOption.point(endXpoint, startYpoint))
                .release()
                .perform();
    }
}
