package br.toxodev.baseClasses;

import br.toxodev.driverFactory.DriverFactory;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class BaseTest {
    private final Logger LOG = Logger.getLogger(">>> BaseTest <<<");

    @Rule
    public TestName testName = new TestName();

    @AfterClass
    public static void closeSession() {
        DriverFactory.killDriver();
    }

    @After
    public void tearDown() {
        this.takeScreenShot();
        DriverFactory.getDriver().resetApp();
    }

    public void takeScreenShot() {
        try {
            File screenShot = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File("target/screenshot/" + testName.getMethodName() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
