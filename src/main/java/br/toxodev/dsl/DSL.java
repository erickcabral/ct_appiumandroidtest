package br.toxodev.dsl;

import org.openqa.selenium.By;

import java.util.logging.Logger;

import static br.toxodev.driverFactory.DriverFactory.getDriver;

public class DSL {
    private final Logger LOG = Logger.getLogger(">>> DSL <<<");

    public void clickElement(By by) {
        getDriver().findElement(by).click();
    }

    public void sendKeys(By by, String text) {
        getDriver().findElement(by).sendKeys(text);
    }

    public String getObjectText(By by) {
        return getDriver().findElement(by).getText();
    }

    public void clickElementByText(String text) {
        clickElement(By.xpath("//*[@text='" + text + "']"));
    }

    public boolean isChecked(By by) {
        return getDriver().findElement(by).getAttribute("checked").equals("true");
    }

    public void selectComboBoxOption(By comboBox, String option) {
        this.clickElement(comboBox);
        clickElement(By.xpath("//android.widget.CheckedTextView[@text='" + option + "']"));
    }
}
